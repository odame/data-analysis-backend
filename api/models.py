from __future__ import unicode_literals

from django.db import models


# Create your models here.
class Data(models.Model):
    name = models.CharField(max_length=50, blank=False, null=False)
    gender = models.IntegerField(blank=False, null=False)
    date_of_birth = models.DateField(blank=False, null=False)
    phone_number = models.CharField(max_length=13, blank=False, null=False)
    house_number = models.CharField(max_length=30, blank=False, null=False)
    time_received = models.DateTimeField(auto_now_add=True)
    time_sent = models.DateTimeField(blank=False, null=False)

    class Meta:
        db_table = 'data'
