from api.models import Data
from api.serializers import DataSerializer, DataFileSerializer
from django.http import HttpResponseBadRequest, HttpResponse
from django.shortcuts import render
from django.utils.datetime_safe import datetime
from rest_framework import status
from rest_framework.generics import ListCreateAPIView
from rest_framework.response import Response
from wsgiref.util import FileWrapper
import tempfile
import csv


MESSAGE_WRAP_CODE = "|"
FIELD_DELIMITER = "~"
MESSAGES_DELIMITER = "`"


class CreateData(ListCreateAPIView):
    serializer_class = DataSerializer
    queryset = Data.objects.all()
    data_fields = [
        'time_sent',
        'name',
        'gender',
        'date_of_birth',
        'phone_number',
        'house_number',
    ]

    def create(self, request, *args, **kwargs):
        message_body = str(request.data['message_body'])
        # we convert message body into individual 'data' messages
        messages_list = message_body.split(MESSAGES_DELIMITER)
        data = []
        errors = {}  # will hold the list of errors. The keys will be the position of the data with the error
        data_count = 0
        for message in messages_list:
            message_length = len(message)
            if message[0] == MESSAGE_WRAP_CODE and message[message_length-1] == MESSAGE_WRAP_CODE:
                actual_message = message[1:message_length-1]  # neglect the MESSAGE_WRAP_CODE
                # we convert message string into individual fields
                actual_message_list = actual_message.split(FIELD_DELIMITER)

                # if the length of the list is not as expected, we add to errors
                if len(actual_message_list) != len(self.data_fields):
                    errors.update({str(data_count): "The number of fields in the data is not as expected"})
                    continue

                i = 0
                actual_data = {}  # will store the fields, mapped onto field names
                for item in actual_message_list:
                    if self.data_fields[i] == "phone_number" and item[0] == "0":
                        item = item.replace("0", "233", 1)
                    actual_data.update({str(self.data_fields[i]): item})
                    i += 1
                actual_data.update({'time_sent': datetime.strptime(
                    actual_data['time_sent'], "%d-%m-%Y %H:%M:%S")})
                actual_data.update({'date_of_birth': datetime.strptime(
                    actual_data['date_of_birth'], "%d-%m-%Y").date()})
                data.append(actual_data)

            else:  # if the data is not surrounded with MESSAGE_WRAP_CODE
                errors.update({str(data_count): "Data is not surrounded with WRAP_CODE"})
            data_count += 1

        if len(errors) != 0:
            return Response({"message": "An error occurred while parsing the data"},
                            status=status.HTTP_406_NOT_ACCEPTABLE)

        serializer = DataSerializer(many=True, data=data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response({"message": "Data has been saved successfully"}, status=status.HTTP_201_CREATED)


def get_file(request):
    queryset = Data.objects.all()
    serializer = DataFileSerializer(queryset, many=True)
    data_set = serializer.data
    try:
        # the names of the fields as they appear in the dataset
        field_names = [
            'name',
            'gender',
            'date_of_birth',
            'phone_number',
            'house_number',
            'time_sent',
            'time_received'
        ]
        # the headings as they will appear in the csv file
        display_headings = {
            'name': "NAME",
            'gender': "GENDER",
            'date_of_birth': "DATE OF BIRTH",
            'phone_number': "PHONE NUMBER",
            'house_number': "HOUSE NUMBER",
            'time_sent': "TIME SENT",
            'time_received': "TIME RECEIVED"
        }

        _file = tempfile.NamedTemporaryFile(suffix='.csv')
        csv_writer = csv.DictWriter(_file, fieldnames=field_names, quoting=csv.QUOTE_ALL)
        csv_writer.writerow(display_headings)  # write the display headers
        csv_writer.writerows(data_set)
        _file.seek(0)
        wrapped_file = FileWrapper(_file)
        response = HttpResponse(wrapped_file, content_type='text/csv')
        response['Content-Length'] = _file.tell()
        response['Content-Disposition'] = 'attachment; filename=%(name)s' % {'name': "filariasis_data.csv"}
        _file.close()

    except Exception as e:
        return HttpResponseBadRequest(e)
    return response
