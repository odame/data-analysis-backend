from api.models import Data
from rest_framework import serializers


class DataSerializer(serializers.ModelSerializer):
    # def __init__(self, *args, **kwargs):
    #     kwargs.update({'many': True})  # we always expect to be dealing with a list
    #     super(DataSerializer, self).__init__(args, kwargs)

    class Meta:
        model = Data
        read_only_fields = ['time_received']


class DataFileSerializer(serializers.ModelSerializer):
    date_of_birth = serializers.DateTimeField(format="%d-%m-%Y")
    time_sent = serializers.DateTimeField(format="%d-%m-%Y %H:%M:%S")
    time_received = serializers.DateTimeField(format="%d-%m-%Y %H:%M:%S")

    class Meta:
        model = Data
        fields = [
            'name',
            'gender',
            'date_of_birth',
            'phone_number',
            'house_number',
            'time_sent',
            'time_received'
        ]
        read_only_fields = fields
